<?php

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Asset\AttachedAssetsInterface;
use \Drupal\Core\Url;
use \Drupal\Core\Path\PathMatcher;
use \Drupal\node\Entity;
use \Drupal\Core\StreamWrapper\PrivateStream;
use \Drupal\file\FileInterface;
use \Drupal\Core\File\FileSystem;
use \Drupal\Core\Entity\FieldableEntityInterface;
use \Drupal\Core\Field\FieldItemInterface;
use \Drupal\Core\Field\FieldItemListInterface;
use \Drupal\Core\TypedData\TypedDataInterface;
use \Drupal\Core\Entity\EntityInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Core\Session\AccountInterface;

module_load_include('inc', 'quadstat_core', 'dataset/dataset');
module_load_include('inc', 'quadstat_core', 'dataset/random.dataset');
module_load_include('inc', 'quadstat_core', 'dataset/excel.dataset');
module_load_include('inc', 'quadstat_core', 'dataset/sas.dataset');
module_load_include('inc', 'quadstat_core', 'dataset/spss.dataset');
module_load_include('inc', 'quadstat_core', 'dataset/file.dataset');
module_load_include('inc', 'quadstat_core', 'dataset/paste.dataset');
module_load_include('inc', 'quadstat_core', 'dataset/empty.dataset');
module_load_include('inc', 'quadstat_core', 'operation/operation');
module_load_include('inc', 'quadstat_core', 'editor/editor');

// Reset the render cache since this interferes with new requests
\Drupal\Core\Cache\Cache::invalidateTags(['rendered']);

/**
 * Run R code
 */
function _quadstat_core_execute_r($commands, $identifier, $is_operation = FALSE) {
  if($is_operation) {
    $prefix = 'operation';
  } else {
    $prefix = 'dataset';
  }
  // Setup up R file with commands to execute
  $cwd = getcwd();
  $tmp_path = \Drupal::service('file_system')->realpath('temporary://');
  chdir($tmp_path);
// Catch errors and also write messages to the warning and error file for this request
$rcode = <<<RCODE
options("width"=200)
warningFile <- file("$prefix-$identifier.warn", "w")
errorFile <- file("$prefix-$identifier.err", "w")
tryCatch({ eval(parse(text = '$commands')) },
    error = function(cond) { writeLines(toString(cond), con = errorFile) },
    warning = function(cond) { writeLines(toString(cond), con = warningFile) }
) 
RCODE;
  $rfile = "$tmp_path/$prefix-$identifier.R";
  file_put_contents($rfile, $rcode);
  $config = Drupal::config('quadstat_core.settings');;
  $r_path = $config->get('quadstat_r_path');
  exec("cd $tmp_path; $r_path CMD BATCH --vanilla --slave $rfile", $output);
  chdir($cwd);
}

/**
 * Implements hook_page_attachments() to attach CSS and JS to specific pages
 */
function quadstat_core_page_attachments(&$attachments) {
  $node = \Drupal::routeMatch()->getParameter('node');
  $user = \Drupal::currentUser();

  // Add CSS for non admin users
  if ($user->id() != 1) {
    $attachments['#attached']['library'][] = 'quadstat/not_admin';
  }

  if (is_object($node) && $node->getType() == 'dataset') {
    // Load SlickGrid JavaScript library with data when using the editor
    $file = $node->get('field_dataset_file')->referencedEntities();
    $is_cont_table = $node->get('field_dataset_cont_table')->value == 0 || $node->get('field_dataset_cont_table')->value == 1 ? $node->get('field_dataset_cont_table')->value : 0;
    if ($file != NULL) {
      $uri = $file[0]->get('uri')->get(0)->value;
      $fid = $file[0]->id();
      
      // Load the webforms for analysis
      $nids = \Drupal::entityQuery('node')->condition('type','webform')->execute();
      $nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);
      $webforms = array();
      $webform_index = 0;
      foreach($nodes as $node) {
        $webforms[$webform_index]['nid'] = $node->id();
        $webforms[$webform_index]['title'] = $node->getTitle();
        $webform_index++;
      }
      $filepath = \Drupal::service('file_system')->realpath($uri);

      $operation_nids = \Drupal::entityQuery('node')->condition('type','operation')->condition('field_views_display', true)->execute();
      $operation_nodes =  \Drupal\node\Entity\Node::loadMultiple($operation_nids);
      $learning = array();
      $learning_index = 0;
      $aliases = array();
      foreach($operation_nodes as $opnode) {
        $alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/' . $opnode->id());
        $learning[$learning_index]['nid'] = $opnode->id();
        $aliases[$opnode->id()] = $alias;
        $learning[$learning_index]['title'] = $opnode->getTitle();
        $learning_index++;
      }

      $row = 0;
      $longest_row = 0;
      $data = [];
      // Begin loading data into Javascript from attached CSV file
      if (($handle = fopen($filepath, "r")) !== FALSE) {
        while (($line = fgetcsv($handle, 0, ",", '"')) !== FALSE) {
          $columns = count($line);
          for ($col = 0; $col < $columns; $col++) {
            $data[$row][$col] = $line[$col];
          }
          if($col > $longest_row) {
            $longest_row = $col;
          }
          $row++;
        }
      }
    }
    fclose($handle);
    $attachments['#attached']['library'][] = 'quadstat_core/slickgrid';
    $attachments['#attached']['drupalSettings']['quadstat_core']['data'] = $data;
    $attachments['#attached']['drupalSettings']['quadstat_core']['longest_row'] = $longest_row;
    $attachments['#attached']['drupalSettings']['quadstat_core']['is_cont_table'] = $is_cont_table;
    $attachments['#attached']['drupalSettings']['quadstat_core']['dataset_fid'] = $fid;
    $attachments['#attached']['drupalSettings']['quadstat_core']['webforms'] = $webforms;
    $attachments['#attached']['drupalSettings']['quadstat_core']['learning'] = $learning;
    $attachments['#attached']['drupalSettings']['quadstat_core']['aliases'] = $aliases;
    $attachments['#attached']['library'][] = 'quadstat_core/dataset';
    $attachments['#attached']['library'][] = 'quadstat_core/quadstat_core';
    $attachments['#attached']['library'][] = 'quadstat_core/codemirror.html';
  }

  // Dataset is being added; add the JavaScript for this page
  $current_path = \Drupal::service('path.current')->getPath();
  if($current_path == '/node/add/dataset') {
    // Adding a dataset doesn't require many libraries
    $attachments['#attached']['library'][] = 'quadstat_core/dataset';
    $attachments['#attached']['library'][] = 'quadstat_core/quadstat';
    $attachments['#attached']['library'][] = 'quadstat_core/codemirror.html';
  }

  // Load datasets preview into JavaScript array
  if (is_object($node) && $node->getType() == 'operation' && $current_path != '/node/add/operation' && strpos($current_path, 'edit') === FALSE) {
    // Get all datasets owned by admin and current user
    $res = db_select('file_managed', 'f')->fields('f')->condition('f.status', 1)->condition('filemime', 'text/csv')->condition('f.uid', array($user->id(), 1),'IN')->orderby('fid', 'DSC')->execute();
    $data = [];
    $map = [];
    $n = 0;
    while($record = $res->fetchAssoc()) {
      $row = 0;
      $longest_row = 0;
      $fid = $record['fid'];
      $file = file_load($fid);
      $uri = $file->get('uri')->get(0)->value;
      // get filesystem path to file
      $filepath = \Drupal::service('file_system')->realpath($uri);

      // Find out whether or not each table is a contingency table
      $rec = db_select('node__field_dataset_file', 'n')->fields('n')->condition('field_dataset_file_target_id', $fid)->execute()->fetchAssoc();
      if($rec['entity_id']) {
        $node = node_load($rec['entity_id']);
        if ($node) {
          $is_cont_table[$n] = !is_null($node->get('field_dataset_cont_table')->value) ? $node->get('field_dataset_cont_table')->value : 0;
        } else { 
          $is_cont_table[$n] = 0;
        }
      }
      $data[$n]['fid'] = $fid;
      // Begin loading data into Javascript from attached CSV file
      if (($handle = fopen($filepath, "r")) !== FALSE) {
        $max_lines = 4;
        while ($row < $max_lines && ($line = fgetcsv($handle, 0, ",", '"')) !== FALSE) {
          $columns = count($line);
          for ($col = 0; $col < $columns; $col++) {
            $data[$n]['val'][$row][$col] = $line[$col];
          }
          if($col > $longest_row) {
            $longest_row = $col;
          }
          $row++;
        }
        $row = 0;
      }
      fclose($handle);
      $data[$n]['longest_row'] = $longest_row;
      $map[$fid] = $n;
      $n++;
    }
    $attachments['#attached']['library'][] = 'quadstat_core/slickgrid';
    $attachments['#attached']['drupalSettings']['quadstat_core']['data'] = $data;
    $attachments['#attached']['drupalSettings']['quadstat_core']['is_cont_table'] = $is_cont_table; 
    $attachments['#attached']['drupalSettings']['quadstat_core']['map'] = $map;
    $attachments['#attached']['library'][] = 'quadstat_core/operation';
    $attachments['#attached']['library'][] = 'quadstat_core/quadstat_core';
    $attachments['#attached']['library'][] = 'quadstat_core/codemirror.html';
  }

  if(is_object($node) && $node->getType() == 'operation' && $current_path != '/node/add/operation' && strpos($current_path, 'edit') !== FALSE) {
    $attachments['#attached']['library'][] = 'quadstat_core/codemirror.r';
    $attachments['#attached']['library'][] = 'quadstat_core/codemirror.html';
  }

  if($current_path == '/node/add/operation') {
    // Adding a dataset doesn't require many libraries
    $attachments['#attached']['library'][] = 'quadstat_core/codemirror.html';
  }

}

/**
 * Implements hook_entity_presave().
 */
function quadstat_core_entity_presave(Drupal\Core\Entity\EntityInterface $entity) {
  // Only react to dataset presaves
  if ($entity->bundle() != 'dataset') {
    return;
  }
  $current_path = \Drupal::service('path.current')->getPath();
  if($entity->isNew() && strpos($current_path, 'quick_clone') === FALSE) {
    _quadstat_create_dataset($entity);
  }
  _quadstat_update_dataset($entity);
}

/**
 * Implements hook_entity_insert() to start process of dataset creation.
 */
function quadstat_core_entity_insert(Drupal\Core\Entity\EntityInterface $entity) {
  if($entity->getEntityType()->id() == 'webform_submission') {
  }
  // Moving onto dataset insertion
  if ($entity->bundle() != 'dataset') {
    return;
  }
  $user = \Drupal::currentUser();
  _quadstat_validate_new_dataset($entity, $user);
}

/**
 * Implements hook_install().
 */
function quadstat_core_install() {
  $r_path = `which R`;
  \Drupal::state()->set('quadstat_core_r_path', $r_path); 
}

/**
 * Implements hook_install().
 */
function quadstat_core_uninstall() {
  \Drupal::state()->delete('quadstat_core_r_path');
}

function _quadstat_core_get_identifier() {
  return rand(10000000, 99999999);
}

function quadstat_core_ajax_render_alter(array &$data) {
  ob_start();
  var_dump($data[2]['selector']);
  $result = ob_get_clean();
  file_put_contents('/tmp/ob', $result);
  if(isset($data[2]) && isset($data[2]['selector']) && strpos($data[2]['selector'], '#webform-submission-') !== FALSE) {
		$data[2]['data'] = _quadstat_cleanup($data[2]['data']);
  }
}

function _quadstat_core_cleanup($output) {
  $output = str_replace('\\\\', '\\', $output);
  $output = str_replace('&amp;quot;', '', $output);
  $output = str_replace('\n', '<br/>', $output);
  $output = str_replace('[code]', '<code>', $output);
  $output = str_replace('[/code]', '</code>', $output);
  $output = str_replace('[p]', '<p>', $output);
  $output = str_replace('[/p]', '</p>', $output);
  $output = str_replace('[strong]', '<strong>', $output);
  $output = str_replace('[/strong]', '</strong>', $output);
  $output = str_replace('[pre]', '<pre>', $output);
  $output = str_replace('[/pre]', '</pre>', $output);
  $output = str_replace('[h2]', '<h2>', $output);
  $output = str_replace('[/h2]', '</h2>', $output);
  $output = str_replace('[img]', '<img src="https://app.quadstat.net', $output);
  $output = str_replace('[/img]', '" alt="Quadstat Statistics Image" />', $output);
  $output = preg_replace("/\[[0-9]+\]/", '', $output);

  return $output;
}

/**
 * Implements hook_file_download().
 */
function quadstat_core_file_download($uri) {
  $account = \Drupal::currentUser();
  $scheme = file_uri_scheme($uri);
  $target = file_uri_target($uri);
  $path_parts = pathinfo($target);
  $ext = $path_parts['extension'];
  // R Filess
  if (strpos($uri, 'public://output/') !== FALSE && $scheme == 'public' &&  $ext == 'R') {
    return ['Content-type' => 'text/plain', 'Content-Disposition' => 'attachment'];
  }
  // Output PDFs
  if (strpos($uri, 'public://output/') !== FALSE && $scheme == 'public' &&  $ext == 'pdf') {
    $file = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => $uri]);
    $fid = key($file);
    if ($fid) {
      $result = db_select('node__field_output_pdf', 'f')->fields('f')->condition('field_output_pdf_target_id', $fid)->execute()->fetchAssoc();
      if($result) {
        $node = node_load($result['entity_id']);
        if ($node->access('view', $account)) {
          return ['Content-type' => 'application/pdf', 'Content-Disposition' => 'inline'];
        }
      }
    }
    return -1;
  }
  // Output HTML files
  if (strpos($uri, 'public://output/') !== FALSE && $scheme == 'public' &&  $ext = 'txt') {
    $file = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => $uri]);
    $fid = key($file);
    if ($fid) {
      $result = db_select('node__field_output_html', 'f')->fields('f')->condition('field_output_html_target_id', $fid)->execute()->fetchAssoc();
      if($result) {
        $node = node_load($result['entity_id']);
        if ($node->access('view', $account)) {
          return ['Content-type' => 'application/txt', 'Content-Disposition' => 'inline'];
        }
      }
    }
    return -1;
  }

  if (strpos($uri, 'public://webform/plot/') !== FALSE && $scheme == 'public' &&  $ext == 'png') {
    $file = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => $uri]);
    $fid = key($file);
    if ($fid) {
      $result = db_select('node__field_output_image', 'f')->fields('f')->condition('field_output_image_target_id', $fid)->execute()->fetchAssoc();
      if($result) {
        $node = node_load($result['entity_id']);
        if ($node->access('view', $account)) {
          return ['Content-type' => 'image/png', 'Content-Disposition' => 'inline'];
        }
      }
    }
    return -1;
  }

  if (strpos($uri, 'public://datasets/') !== FALSE) {
    if ($scheme == 'public' &&  $ext == 'csv')  {
      $result = db_select('file_managed', 'f')->fields('f')->condition('uri', $uri)->execute()->fetchAssoc();
      $result = db_select('node__field_dataset_file', 'n')->fields('n')->condition('field_dataset_file_target_id', $result['fid'])->execute()->fetchAssoc();
      $node = node_load($result['entity_id']);
      if($node&&$node->access('view', $account)) {
        return array("Content-disposition" => "attachment");
      }
    }
    return -1;
  }
  return NULL;
}

/*
 * Implements hook_node_access().
 */
function quadstat_core_node_access(\Drupal\node\NodeInterface $node, $op, \Drupal\Core\Session\AccountInterface $account) {
  if ($node->getType() == 'output') {
    if (isset($node->field_output_dataset_ref->getValue()[0])) {
      $nid = $node->field_output_dataset_ref->getValue()[0]['target_id'];
    }
    if (isset($nid) && !is_int($nid)) {
      return AccessResult::neutral();
    }
    if (isset($nid)) {
      $n = node_load($nid);
    }
    if (isset($n) && !$n && $n->getType() == 'dataset') {
      if($n->access('view', $account)) {
        return AccessResult::allowed();
      } else {
        //return AccessResult::forbidden();
      }
    }
  }
  return AccessResult::neutral();
}

function quadstat_core_contact_submit(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $url = Url::fromRoute('contact.site_page');
    $form_state->setRedirectUrl($url);
}

function quadstat_core_user_logout($account) {
  $url = "/user/login";
  $response = new Symfony\Component\HttpFoundation\RedirectResponse($url);
  $response->send();
}

function quadstat_core_user_login($account) {
  $current_path = \Drupal::service('path.current')->getPath();
  if (strpos($current_path, '/user/reset') === FALSE) {
    drupal_set_message('You have been successfully logged into Quadstat.');
    $url = "/user/" . $account->id() . "/edit";
    $response = new Symfony\Component\HttpFoundation\RedirectResponse($url);
    $response->send();
    return;
  }
}
