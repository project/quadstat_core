<?php

use \Drupal\Core\Url;
use \Drupal\Core\Path\PathMatcher;
use Drupal\user\Entity\User;
use \Drupal\node\Entity;
use Drupal\node\Entity\Node;


/**
 * Tell Drupal that we have an additional validation function.
 */
function quadstat_form_alter(array &$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {

  // Relate group content only owned by current user
  if ($form_id == 'group_content_group-group_node-dataset_add_form' || $form_id == 'group_content_group-group_node-page_add_form') {
    $form['#title'] = 'Add Group Content';
    $form['actions']['submit']['#validate'][] = 'quadstat_group_content_form_validate';
  }

  //contact page form
  if ($form_id == 'contact_message_feedback_form') {
    $form['actions']['submit']['#submit'][] = 'quadstat_contact_submit';
  }

 if ($form_id == 'user_register_form') {
   $form['actions']['submit']['#submit'][] = '_quadstat_register_submit';
 }

  $uid = \Drupal::currentUser()->id();
  $user = \Drupal\user\Entity\User::load($uid);
  $current_path = \Drupal::service('path.current')->getPath();

  // Validate R request
  if (strpos($form_id, 'webform_submission_') !== FALSE) {
    $form['actions']['submit']['#validate'][] = 'quadstat_operation_form_validate';
    if ($uid < 1) {
      $form['elements']['license']['#attributes']['disabled']  = 'disabled';
      $form['elements']['license']['#default_value']  = 'public_domain';
    }
  }

  if ($form_id == 'views_exposed_form' && ($form['#id'] == 'views-exposed-form-my-datasets-block-1' || $form['#id'] == 'views-exposed-form-dataset-block-1')) {
    $nodes = db_select('node_field_data', 'n')->fields('n')->condition('type', 'dataset')->condition('n.uid', array($user->id(), 1),'IN')->orderby('title', 'ASC')->execute(); 
    $options = array('' => '- Curated Data -');
    $options = array('Example Data' => 'Example Data');
    while($rec = $nodes->fetchAssoc()) {
      $rec['title'] = preg_replace("/R Dataset \/ Package (.*) \/ /", '', $rec['title']);
      $options[$rec['title']] = substr($rec['title'], 0, 15);
    }
    unset($options['Inline Data']);
    $form['dataset_selected'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => '',
    );
    if ($form['#id'] == 'views-exposed-form-dataset-block-1') {
      $form['operation_dataset_edit'] = array(
         '#type' => 'button',
         '#value' => 'Full View ',
      );
    }

    $nodes = db_select('node_field_data', 'n')->fields('n')->condition('type', 'dataset')->condition('n.uid', $user->id(),'=')->orderby('title', 'ASC')->execute();
    $options = array('' => '- My Data -','Inline Data' => 'Add Inline Data');
    while($rec = $nodes->fetchAssoc()) {
      $options[$rec['title'] . '-' . $rec['nid']] = substr($rec['title'], 0, 15);
    }
    $form['dataset_selected_custom'] = array(
       '#type' => 'select',
       '#options' => $options,
       '#default_value' => 'Example Data',
       '#attributes' => array('class' => array('view-dataset-selected-custom')),
    );
    $form['dataset_last_selected'] = array(
       '#type' => 'hidden',
       '#default_value' => 'curated',
    );
  }

}

#function _quadstat_register_submit(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
#  $redirect_path = "/dash";
#  $url = url::fromUserInput($redirect_path);  
#  $form_state->setRedirectUrl($url);
#}

/**
 * Make sure user owns content before adding to group
 */
function quadstat_group_content_form_validate(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $entity = $form_state->getValue('entity_id');
  $entity_id = $entity[0]['target_id'];
  $node = \Drupal::entityTypeManager()->getStorage('node')->load($entity_id);
  $entity_owner = $node->getOwnerId();
  $user = User::load(\Drupal::currentUser()->id());
  if ($entity_owner != $user->id()) {
    $form_state->setErrorByName('entity_id_0_target_id', t('Content to be added to group must be owned by the current user.'));
  } else {
    $form_state->setTemporaryValue('entity_validated', TRUE);
  }
}
/**
 * Execute the command from the webform
 */
function quadstat_operation_form_validate(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  // Get the values the user submitted
  $user = \Drupal::currentUser();
  $form_input = $form_state->getValues();
  $command_url = '';
  $dataset_setup_cmd = _quadstat_setup_dataset($form, $form_state, $form_input, $command_url);
  if(!isset($form_input['r'])) {
    // This is not an R operation
    return;
  }
  if(!_quadstat_is_valid($form_input)) {
    // Invalid inputs
    $form_input['output'] = '[p]Please select the correct number of column variables.[p]';
    if(isset($form_input['image'])) {
      $form_input['image'] = 1121;
    }
    $form_state->setValues($form_input);
    return;
 }
  $command = $form_input['r'];
  $fid = isset($form_input['x']) ? $form_input['x'] : FALSE;
  if ($fid) {
    $file = \Drupal\file\Entity\File::load($fid);
  } else {
    $file = FALSE;
  }
  // dl stands for Dollar Literal (do not replace dollar signs with PHP variables because R is going to use them) 
  if(!isset($form_input['dl'])) {
    // Replace each variable with the user input
    foreach($form_input as $key => $input) {
      if(is_string($key) && is_string($form_input[$key])) {
        $command = str_replace('$' . $key, $form_input[$key], $command);
      }
    }
  }
  $identifier = _quadstat_core_get_identifier();
  if(array_key_exists('image', $form_input)) {
    // User wants an image
    $img_setup = _quadstat_setup_img($form, $form_state, $identifier);
    // $img_save has dev.off()
    $img_save = _quadstat_save_img($form, $form_state, $identifier);
  } else {
    // No image will be generated
    $img_setup = $img_save = '';
  }
  $command = $dataset_setup_cmd . $img_setup . $command .$img_save;
  _quadstat_execute_r($command, $identifier, TRUE);
  $tmp_path = \Drupal::service('file_system')->realpath('temporary://');
  // Let user know of any errors or warnings while processing dataset request
  if(is_file($error_file = "$tmp_path/operation-$identifier.err") && filesize($error_file) > 0) {
    drupal_set_message(file_get_contents($error_file), 'error');
  }
  if(is_file($warn_file = "$tmp_path/operation-$identifier.warn") && filesize($warn_file) > 0) {
    drupal_set_message(file_get_contents($warn_file), 'warning');
  }
  $output_file = "$tmp_path/operation-$identifier.Rout";
  $foo = file_get_contents($output_file);
  $foo = str_replace('"', '', $foo);
  //$foo = preg_replace("/\"\n/", "\n", $foo);
  file_put_contents($output_file, $foo);
  // Remove the trailing lines
  $lines = file($output_file); 
  $last = sizeof($lines); 
  $i = 0;
  $max = 3;
  if(array_key_exists('image', $form_input)) {
    $max = 5;
  }
  while($i < $max) {
    array_pop($lines);
    $i++;
  }
  $output = implode('', $lines);
  if ($file) {
    // replace absolute path to dataset with url
    $form_input['r'] = preg_replace("/\"\/var(.*)\.csv\"/", '"' . file_create_url($file->get('uri')->get(0)->value) . '"', $command);
   } else {
    // dataset
    $form_input['r'] = $command;
  }
  $form_input['output'] = $output;
  $data = false;
  $i = 1;
  $im_command = "cd $tmp_path; convert ";
  $found_img = FALSE;
  // Cycle through all the images, we are going to append them together into a single image
  while (is_file("$tmp_path/operation-$identifier-$i.png")) {
    $im_command .= "operation-$identifier-$i.png ";
    $found_img = TRUE;
    $i++;
  }
  // Check for ggplots
  $ggplot = FALSE;
  if (is_file("$tmp_path/ggplot.png")) {
    $im_command .= "ggplot.png  ";
    // $i keeps track of how many images we have
    $i++;
    $found_img = TRUE;
    $ggplot = TRUE;
  }
  if($found_img) {
    // execute the command
    $im_command .= "-append operation-$identifier.png";
    if($ggplot) {
      system("cd /tmp;convert -resize 455x455 ggplot.png ggplot.png;");
    }
    system($im_command);
    if (is_file("$tmp_path/ggplot.png")) {
      unlink("$tmp_path/ggplot.png");
    }
  }
  // Now save the file to the Drupal database
  $data = false;
  if (is_file("$tmp_path/operation-$identifier.png")) {
    $data = file_get_contents("$tmp_path/operation-$identifier.png");
  }
  if ($data) {
    $username = $user->getAccountName();
    if($username == '') {
      $username = 'anonymous';
    }
    $png_dir = "public://webform/plot/$username";
    if(!is_dir(\Drupal::service('file_system')->realpath($png_dir))) {
      \Drupal::service('file_system')->mkdir($png_dir);
    }
    if (file_prepare_directory($png_dir)) {
      $file = file_save_data($data, "$png_dir/operation-$identifier.png", FILE_EXISTS_RENAME);
      if($file) {
        $form_input['image'] = $file->get('fid')->get(0)->value;
      }
    }
  } else if(isset($form_input['image'])) {
    // No image available
    $form_input['image'] = 1121;
  }
  _quadstat_pdf($form, $form_state, $form_input, $command_url);
  $form_state->setValues($form_input);
}

function _quadstat_setup_dataset(&$form, $form_state, $form_input, &$command_url) {
  $inline = isset($form_input['inline_data']) ? $form_input['inline_data'] : NULL;
  $inline = preg_replace("/[^0-9\-\.\,\n\(\)]/", '', $inline);
  $command = '';

  $fid = isset($form_input['x']) ? $form_input['x'] : NULL;
  $v = isset($form_input['v']) ? $form_input['v'] : NULL;
  $v1 = isset($form_input['v1']) ? $form_input['v1'] : NULL;
  $v2 = isset($form_input['v2']) ? $form_input['v2'] : NULL;
  $v3 = isset($form_input['v3']) ? $form_input['v3'] : NULL;

  if ($inline) {
    $count = count($inline = explode("\n", $inline));
    if (is_array($inline) && count($inline[0]) && !isset($inline[1])) {
      $inline[0] = substr($inline[0], 1);
      $command .= "# Create the vector\r\nX <- cbind(X = c($inline[0]));\r\n\r\n";
      $command .= "# Some commands require the data as an R data frame\r\ndfr <- as.data.frame(cbind(X = c($inline[0])));\r\n\r\n";
      $command .= "# Some applications use the raw variable\r\nraw <- cbind(X = c($inline[0]));\r\n\r\n";
      $command_url = $command;
    } else {
      $inline[0] = substr($inline[0], 1);
      $inline[1] = substr($inline[1], 1);
      $command .= "# Create the vectors\r\nX <- cbind(X = c($inline[0]), Y = c($inline[1]));\r\n\r\n";
      $command .= "# Some commands require the data as an R data frame\r\ndfr <- as.data.frame(cbind(X = c($inline[0]), Y = c($inline[1])));\r\n\r\n";
      $command .= "# Some applications use the raw variable\r\nraw <- cbind(X = c($inline[0]), Y = c($inline[1]));\r\n\r\n";
      $command_url = $command;
    }
    return $command;
  }
  if(is_null($fid)) {
    // A dataset is not involved; do nothing
    return '';
  }
  $user = \Drupal::currentUser();
  $file = file_load($fid);
  if ($file) {
    $uri = $file->get('uri')->get(0)->value;
  }
  $filepath = \Drupal::service('file_system')->realpath($uri); 
  $cu = str_replace('http://', 'https://', file_create_url($uri));
  $command_url = "# Read the dataset into identical variables for later use\r\n";
  $command_url .= "X <- read.table(\"$cu\", sep=\",\", header = TRUE, fill = TRUE, quote = \"\");\r\n";
  $command_url .= "dfr <- read.table(\"$cu\", sep=\",\", header = TRUE, fill = TRUE, quote = \"\");\r\n";
  $command_url .= "raw <- read.table(\"$cu\", sep=\",\", header = TRUE, fill = TRUE, quote = \"\");\r\n\r\n";

  $command .= "X <- read.table(\"$filepath\", sep=\",\", header = TRUE, fill = TRUE, quote = \"\");\n";
  $command .= "dfr <- read.table(\"$filepath\", sep=\",\", header = TRUE, fill = TRUE, quote = \"\");\n";
  $command .= "raw <- read.table(\"$filepath\", sep=\",\", header = TRUE, fill = TRUE, quote = \"\");\n";
  if (!is_null($v) && $v) {
    // The user has chosen entire dataset
    $command .= "colNam <- colnames(X);\n";
    $command .= "X <- cbind(X);\n";
    $command .= "colnames(X) <- colNam;\n";
  } else if ((!is_null($v1) && $v1 != '-1') && (!is_null($v2) && $v2 != '-1')  && (!is_null($v3) && $v3 != '-1')) {
    // The user has chosen 3 vectors
    $command .= "colNam <- colnames(X);\n";
    $command .= "X <- cbind(X[,$v1], X[,$v2], X[,$v3]);\n";
    $command .= "colnames(X) <- c(colNam[$v1], colNam[$v2], colNam[$v3]);\n";
    $command .= "dfr <- as.data.frame(c(dfr[$v1], dfr[$v2], dfr[$v3]))\n";
    $command_url .= "# Get the columns names\ncolNam <- colnames(X);\n# Select the column(s)\r\nX <- cbind(X[,$v1], X[,$v2], X[,$v3]);\r\n\r\n# Create a matrix-like object out of the column names\ncolnames(X) <- c(colNam[$v1], colNam[$v2], colNam[$v3]);\r\n\r\n" . "# Some commands require the data as an R data frame\r\ndfr <- as.data.frame(c(dfr[$v1], dfr[$v2], dfr[$v3]))\r\n\r\n";
  } else if ((!is_null($v1) && $v1 != '-1') && (!is_null($v2) && $v2 != '-1')) {
    // The user has chosen 2 vectors
    $command .= "colNam <- colnames(X);\n";
    $command .= "X <- cbind(X[,$v1], X[,$v2]);\n";
    $command .= "colnames(X) <- c(colNam[$v1], colNam[$v2]);\n";
    $command .= "dfr <- as.data.frame(c(dfr[$v1], dfr[$v2]))\n";
    $command_url .= "# Get the column names\ncolNam <- colnames(X);\n# Select the column(s)\r\nX <- cbind(X[,$v1], X[,$v2]);\r\n\r\n# Create a matrix-like object out of the column names\ncolnames(X) <- c(colNam[$v1], colNam[$v2]);\r\n\r\n" . "# Some commands require the data as an R data frame\r\ndfr <- as.data.frame(c(dfr[$v1], dfr[$v2]))\r\n\r\n";
  }
  else if (!is_null($v1) && $v1 != '-1') {
    // The user has chosen 1 vector
    $command .= "colNam <- colnames(X);\n";
    $command .= "X <- cbind(X[,$v1]);\n";
    $command .= "colnames(X) <- c(colNam[$v1]);\n";
    $command .= "dfr <- as.data.frame(c(dfr[$v1]))\n";
    $command_url .= "# Get the column names\ncolNam <- colnames(X);\n# Select the column(s)\r\nX <- cbind(X[,$v1]);\r\n\r\n# Create a matrix-like object out of the column names\ncolnames(X) <- c(colNam[$v1]);\r\n\r\n" . "# Some commands require the data as an R data frame\r\ndfr <- as.data.frame(c(dfr[$v1]))\r\n\r\n";
  } else {
    // The user has not selected any vectors, use the whole dataset. Don't need to do anything.
  }
  return $command;
}

function _quadstat_setup_img(&$form, $form_state, $identifier) {
  $tmp_path = \Drupal::service('file_system')->realpath('temporary://');
  $setup_img_cmd = "png(file = \"$tmp_path/operation-$identifier-%01d.png\", bg = \"transparent\");\n";
  return $setup_img_cmd;
}

function _quadstat_save_img(&$form, $form_state, $identifier) {
  return $save_img_cmd = "dev.off();\n";
}

function _quadstat_pdf(&$form, $form_state, &$form_input, &$command_url) {
  $pdf = isset($form_input['pdf']) ? $form_input['pdf'] : NULL;
  if ($pdf) {
    $content = $form_input['output'];
    $image_fid = $content_img = isset($form_input['image']) ? $form_input['image'] : '';
  
    $image_file = file_load($content_img);
    if ($image_file) {
      $content_img = \Drupal::service('file_system')->realpath($image_file->get('uri')->get(0)->value);
      $content_img = '<img src="' . $content_img . '" alt="Quadstat Statistics Image" />';
    }
    $content = _quadstat_cleanup($content);
  
    $module_handler = \Drupal::service('module_handler');
    $quadstat_path = $module_handler->getModule('quadstat')->getPath();
    require_once("$quadstat_path/templates/pdf.html.php");
    $uid = \Drupal::currentUser()->id();
    if ($uid == 0) {
      $name = 'Anonymous';
    } else {
      $user = \Drupal\user\Entity\User::load($uid);
      $name = $user->getUsername();
    }
    $rand = rand(10000,99999);
    $html_path = "public://output/$name/html/";
    $pdf_path = "public://output/$name/pdf/";
    file_prepare_directory($html_path, FILE_CREATE_DIRECTORY);
    file_prepare_directory($pdf_path, FILE_CREATE_DIRECTORY);
    $html_file = file_save_data($html, "public://output/$name/html/output-$rand.html.txt", FILE_EXISTS_RENAME);
    $tmp_path = \Drupal::service('file_system')->realpath('temporary://');
    file_put_contents($tmp_path . "/output-$rand.html", $html);
    exec("cd $tmp_path; xvfb-run wkhtmltopdf --javascript-delay 1500 output-$rand.html output-$rand.pdf");
    $pdf_contents = file_get_contents("$tmp_path/output-$rand.pdf");
    $pdf_file = file_save_data($pdf_contents, "public://output/$name/pdf/output-$rand.pdf", FILE_EXISTS_RENAME);
    $form_input['html'] = $html_file->get('fid')->get(0)->value;
    $form_input['pdf_file'] = $pdf_file->get('fid')->get(0)->value;
  }

  $entity = \Drupal::entityTypeManager()->getStorage('webform')->load($form['#webform_id']);
  $node = \Drupal::routeMatch()->getParameter('node');
  $nid = FALSE;
  if ($node instanceof \Drupal\node\NodeInterface) {
    // You can get nid and anything else you need from the node object.
    $nid = $node->id();
  }
  _quadstat_r_file($form, $form_state, $form_input, $command_url);
  $output_nid = _quadstat_create_output($entity, $form_input, $form['#webform_id'], $nid);
  $output_node = node_load($output_nid);
  $shorten_option = isset($form_input['shorten_option']) ? $form_input['shorten_option'] : NULL;
  if ($shorten_option) {
    $shorten = shorten_url("https://app.quadstat.net/node/$output_nid");
    $form_input['shorten'] = $shorten;
    $output_node->set('field_output_shorten', $shorten);
    $output_node->save();
  }
}

function _quadstat_r_file(&$form, $form_state, &$form_input, $command_url) {
  $r_file_checkbox = isset($form_input['r_file_checkbox']) ? $form_input['r_file_checkbox'] : NULL;
  if ($r_file_checkbox) {

    // dl stands for Dollar Literal (do not replace dollar signs with PHP variables because R is going to use them) 
    if (!isset($form_input['dl'])) {
      // Replace each variable with the user input
      foreach($form_input as $key => $input) {
        if(is_string($key) && is_string($form_input[$key])) {
          $form_input['r_file_helper'] = str_replace('$' . $key, $form_input[$key], $form_input['r_file_helper']);
        }
      }
    }

    $content = $command_url . $form_input['r_file_helper'];

    $uid = \Drupal::currentUser()->id();
    if ($uid == 0) {
      $name = 'Anonymous';
    } else {
      $user = \Drupal\user\Entity\User::load($uid);
      $name = $user->getUsername();
    }

    $rand = rand(1000000,9999999);
    $r_file_path = "public://output/$name/r/";
    file_prepare_directory($r_file_path, FILE_CREATE_DIRECTORY);
    $r_file = file_save_data($content, "public://output/$name/r/Quadstat-$rand.R", FILE_EXISTS_RENAME);
    $form_input['r_file'] = $r_file->get('fid')->get(0)->value;
  }

}

function _quadstat_create_output($entity, $form_input, $webform_id, $nid) {
  // Create output node, Read-in machine name to label 

  $filey = false;
  $label = $entity->get('title');
  $title = 'Quadstat Output - ' . $label;
  $output = _quadstat_cleanup($form_input['output']);

  $uid =  \Drupal::currentUser()->id();
  $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

  $name = $user->get('field_name')->value;
  if($uid == 0) {
    $name = 'Anonymous';
  }
  if (!strlen($name)) {
    $name = $user->get('name')->value;
  }

  // Load the dataset we used
  if (isset($form_input['x']) && $form_input['x'] == 1162) {
    $dataset = $form_input['inline_data'];
  } else {
    $dataset = isset($form_input['x']) ? file_load($form_input['x']) : FALSE;
    if ($dataset) {
      $dataset = '<a href="' . file_create_url($dataset->get('uri')->get(0)->value) . '">' . $dataset->get('uri')->get(0)->value . '</a>';
    }
  }
  if (isset($form_input['image'])) {
    $file = file_load($form_input['image']);
    if($file) {
      $filey = ['target_id' => $file->get('fid')->get(0)->value, 'alt' => 'Quadstat Statistical Image using the ' . $label . ' web application found on Quadstat.net', 'title' => 'Quadstat Statistics Image of a ' . $label];
    }
  }

  // Attach the R Command File
  if (isset($form_input['r_file'])) {
    $file = file_load($form_input['r_file']);
    if($file) {
      $filez = ['target_id' => $file->get('fid')->get(0)->value, 'alt' => 'Quadstat R Command File using the ' . $label . ' web application found on Quadstat.net', 'title' => 'Quadstat R Command File of ' . $label];
    } else {
      $filez = null;
    }
  }
  // Get the Dataset NID used
  if (isset($form_input['x'])) {
    $result = db_select('node__field_dataset_file', 'n')->fields('n')->condition('field_dataset_file_target_id', $form_input['x'])->execute()->fetchAssoc();
    $n = \Drupal\node\Entity\Node::load($result['entity_id']);
  } else {
    $n = FALSE;
  }
  // Check if PDF file was requested
  if ($form_input['pdf_file']) {
    $node = Node::create(['title' => $title, 'field_app' => $nid, 'field_output_dataset_ref' => is_object($n) ? $n->id() : '', 'type' => 'output', 'body' => ['value' => $output, 'format' => 'output'], 'uid' => $uid, 'field_output_html' => $form_input['html'], 'field_output_pdf' => $form_input['pdf_file'], 'field_dataset_license' => $form_input['license'], 'field_output_copyright_owner' => $name, 'field_output_dataset' => ['value' => $dataset, 'format' => 'output']]);
  } else {
    // Some applications don't require datasets
    if (!is_object($n)) {
      $field_output_dataset_ref = '';
    }
    else {
      $field_output_dataset_ref = $n->id();
    }
    $node = Node::create(['title' => $title, 'field_app' => $nid, 'field_output_dataset_ref' => $field_output_dataset_ref, 'type' => 'output', 'body' => ['value' => $output, 'format' => 'output'], 'uid' => $uid, 'field_dataset_license' => $form_input['license'], 'field_output_copyright_owner' => $name, 'field_output_dataset' => ['value' => $dataset, 'format' => 'output']]);
  }
  if ($filey) {
    $node->field_output_image->setValue($filey);
  }
  if ($filez) {
    $node->field_r_file->setValue($filez);
  }
  $node->save();
  return $node->id();
}

function _quadstat_is_valid($form_input) {
  // Check for inline data
  if (isset($form_input['x']) && $form_input['x'] == '1162') {
    if (strlen($form_input['inline_data'])) {
      return TRUE;
    } else {
      return FALSE;
    }
  }
  if ((isset($form_input['v']) && $form_input['v'] > 0) || !isset($form_input['v1'])) {
    // Check entire dataset
    return TRUE;
  } else if (($form_input['v1'] == '-1' && !isset($form_input['v2'])) || (isset($form_input['v2']) && $form_input['v2'] == -1)) {
    // One or two columns required
    return FALSE;
  } else if (isset($form_input['v2']) && $form_input['v2'] == '-1') {
    // Two columns required
    return FALSE;
  } else if (isset($form_input['v1']) && $form_input['v1'] == '-1') {
    // One column required
    return FALSE;
  } else {
    // All's good
    return TRUE;
  }
}
