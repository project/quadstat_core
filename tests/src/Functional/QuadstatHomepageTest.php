<?php

namespace Drupal\Tests\quadstat\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests to ensure Quadstat.com Homepage is reachable
 *
 * @group quadstat_ui
 */
class QuadstatHomepageTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'user'];

  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

  }

  /**
   * Tests that the reaction rule listing page works.
   */
  public function testHomepage() {

    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('What Is Quadstat?');
  }
}
