/**
 * @file
 * Javascript behaviors for viewing data.
 */

/* global Drupal, jQuery, document */

(function ($, Drupal, document) {

  'use strict';

  /**
   * Attaches behaviors for Quadstat.
   */
  Drupal.behaviors.quadstat_core = {
    attach: function (context, settings) {

      $('#quadstat-html').css('display', 'none !important');
        $('#quadstat-pdf').css('display', 'none !important');
      // Launch CodeMirror HTML Syntax Highlighting
      if($("#edit-body-0-value").length && $('.CodeMirror').length == 0) {
        var editor = CodeMirror.fromTextArea(document.getElementById('edit-body-0-value'), {mode: 'htmlmixed', lineNumbers: true, textWrapping: true});
      }

      $("input[name=field_dataset_cont_table]").change(function () {
        switch($(this).val()) {
          case "0": {
            $('#edit-field-dataset-cont-table--wrapper--description').css('display', 'none')
            $('#edit-field-dataset-random-columns-wrapper').css('display', 'block')
            $('#edit-field-dataset-random-rows-wrapper').css('display', 'block')
            $('#edit-field-dataset-excel-sheet-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-column-names-wrapper').css('display', 'none')
            break;
          }
          case "1": {
            $('#edit-field-dataset-cont-table--wrapper--description').css('display', 'block')
            $('#edit-field-dataset-random-columns-wrapper').css('display', 'none')
            $('#edit-field-dataset-random-rows-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-sheet-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-column-names-wrapper').css('display', 'none')
            break;
          }
        }
      });

      // Show or hide fields when adding a dataset based on user selection
      $("input[name=field_dataset_input_method]").change(function () {
        switch($(this).val()) {
          case "random": {
            if($("input[name=field_dataset_input_method]").val() == 1) {
              $('#edit-field-dataset-cont-table--wrapper--description').css('display', 'none')
              $('#edit-field-dataset-random-columns-wrapper').css('display', 'none')
              $('#edit-field-dataset-random-rows-wrapper').css('display', 'none')
              $('#edit-field-dataset-excel-sheet-wrapper').css('display', 'none')
              $('#edit-field-dataset-excel-column-names-wrapper').css('display', 'none')
              $('#edit-field-dataset-cont-table-wrapper').css('display', 'none')
             
            }
            else {
              $('#edit-field-dataset-cont-table--wrapper--description').css('display', 'block')
              $('#edit-field-dataset-random-columns-wrapper').css('display', 'block')
              $('#edit-field-dataset-random-rows-wrapper').css('display', 'block')
              $('#edit-field-dataset-excel-sheet-wrapper').css('display', 'none')
              $('#edit-field-dataset-excel-column-names-wrapper').css('display', 'none')
              $('#edit-field-dataset-cont-table-wrapper').css('display', 'none')
            }
            $('#edit-field-dataset-file-wrapper').css('display', 'none')
            $('#edit-field-dataset-paste-wrapper').css('display', 'none')
            $('#edit-field-dataset-header-wrapper').css('display', 'none')
            $('#edit-field-dataset-separator-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-sheet-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-column-names-wrapper').css('display', 'none')
            $('#edit-field-dataset-cont-table-wrapper').css('display', 'none')
            break;
          }

          case "file": {
            $('#edit-field-dataset-random-columns-wrapper').css('display', 'none')
            $('#edit-field-dataset-random-rows-wrapper').css('display', 'none')
            $('#edit-field-dataset-file-wrapper').css('display', 'block')
            $('#edit-field-dataset-paste-wrapper').css('display', 'none')
            $('#edit-field-dataset-header-wrapper').css('display', 'block')
            $('#edit-field-dataset-separator-wrapper').css('display', 'block')
            $('#edit-field-dataset-excel-sheet-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-column-names-wrapper').css('display', 'none')
            $('#edit-field-dataset-cont-table-wrapper').css('display', 'block')
            break;
          }

          case "paste": {
            $('#edit-field-dataset-random-columns-wrapper').css('display', 'none')
            $('#edit-field-dataset-random-rows-wrapper').css('display', 'none')
            $('#edit-field-dataset-file-wrapper').css('display', 'none')
            $('#edit-field-dataset-paste-wrapper').css('display', 'block')
            $('#edit-field-dataset-header-wrapper').css('display', 'block')
            $('#edit-field-dataset-separator-wrapper').css('display', 'block')
            $('#edit-field-dataset-excel-sheet-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-column-names-wrapper').css('display', 'none')
            $('#edit-field-dataset-cont-table-wrapper').css('display', 'block')
            break;
          }

          case "empty": {
            $('#edit-field-dataset-random-columns-wrapper').css('display', 'none')
            $('#edit-field-dataset-random-rows-wrapper').css('display', 'none')
            $('#edit-field-dataset-file-wrapper').css('display', 'none')
            $('#edit-field-dataset-paste-wrapper').css('display', 'none')
            $('#edit-field-dataset-header-wrapper').css('display', 'none')
            $('#edit-field-dataset-separator-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-sheet-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-column-names-wrapper').css('display', 'none')
            $('#edit-field-dataset-cont-table-wrapper').css('display', 'none')
            break;
          }
          case "excel": {
            $('#edit-field-dataset-random-columns-wrapper').css('display', 'none')
            $('#edit-field-dataset-random-rows-wrapper').css('display', 'none')
            $('#edit-field-dataset-file-wrapper').css('display', 'block')
            $('#edit-field-dataset-paste-wrapper').css('display', 'none')
            $('#edit-field-dataset-header-wrapper').css('display', 'none')
            $('#edit-field-dataset-separator-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-sheet-wrapper').css('display', 'block')
            $('#edit-field-dataset-excel-column-names-wrapper').css('display', 'block')
            $('#edit-field-dataset-cont-table-wrapper').css('display', 'none')
            break;
          }
          case "spss": {
            $('#edit-field-dataset-random-columns-wrapper').css('display', 'none')
            $('#edit-field-dataset-random-rows-wrapper').css('display', 'none')
            $('#edit-field-dataset-file-wrapper').css('display', 'block')
            $('#edit-field-dataset-paste-wrapper').css('display', 'none')
            $('#edit-field-dataset-header-wrapper').css('display', 'none')
            $('#edit-field-dataset-separator-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-sheet-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-column-names-wrapper').css('display', 'none')
            $('#edit-field-dataset-cont-table-wrapper').css('display', 'none')
            break;
          }
          case "sas": {
            $('#edit-field-dataset-random-columns-wrapper').css('display', 'none')
            $('#edit-field-dataset-random-rows-wrapper').css('display', 'none')
            $('#edit-field-dataset-file-wrapper').css('display', 'block')
            $('#edit-field-dataset-paste-wrapper').css('display', 'none')
            $('#edit-field-dataset-header-wrapper').css('display', 'none')
            $('#edit-field-dataset-separator-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-sheet-wrapper').css('display', 'none')
            $('#edit-field-dataset-excel-column-names-wrapper').css('display', 'none')
            $('#edit-field-dataset-cont-table-wrapper').css('display', 'none')
            break;
          }
        }
      });

      if($('#node-dataset-form').length > 0) {
        return;
      }
      var dataView;
      var grid;
      var options;
      var commandQueue = [];
      var columnFilters = {};
      var data = [];
 
      // Define options for grid
      var options = {
        editable: true,
        enableAddRow: false,
        enableCellNavigation: true,
        asyncEditorLoading: true,
        forceFitColumns: false,
        topPanelHeight: 25,
        enableColumnReorder: false,
        explicitInitialization: true,
        showHeaderRow: true,
        headerRowHeight: 30,
        autoedit: true,
        editCommandHandler: queueAndExecuteCommand
      };

      // Setup columns
      if (drupalSettings.quadstat_core.is_cont_table == null || typeof drupalSettings.quadstat_core.is_cont_table == 'undefined' || drupalSettings.quadstat_core.is_cont_table == 0) {
        // Setup columns for contingency tables
        var columns = [{id:0, name:"Rows", field:0, cssClass:"cell-title", formatter: function(row){return row+1}}];
      } else {
        var columns = [{id:0, name:"Rows", field:0, cssClass:"cell-title", editor:Slick.Editors.Text}];
      }
      if(typeof drupalSettings.quadstat_core.data[0] != 'undefined') {
        for(var i = 0; i < drupalSettings.quadstat_core.longest_row || i < 35; i++) {
          columns.push({id:i+1, name:typeof drupalSettings.quadstat_core.data[0][i] !== 'undefined' ? drupalSettings.quadstat_core.data[0][i] : '', field:i+1, editor:Slick.Editors.Text});
        }
      }

      function queueAndExecuteCommand(item, column, editCommand) {
        // Push the change to the commandQueue variable;
        commandQueue.push(editCommand);
        editCommand.execute();
      }

      function filter(item) {
        for (var columnId in columnFilters) {
          if (columnId !== undefined && columnFilters[columnId] !== "") {
            var c = grid.getColumns()[grid.getColumnIndex(columnId)];
            if (item[c.field] != columnFilters[columnId]) {
              return false;
            }
          }
        }
        return true;
      }

      function setItems(data, objectIdProperty) {
        if (objectIdProperty !== undefined) {
          idProperty = objectIdProperty;
        }
        items = data;
        refreshIdxById();
        refresh();
      }

      if (drupalSettings.quadstat_core.is_cont_table == null || typeof drupalSettings.quadstat_core.is_cont_table == 'undefined' || drupalSettings.quadstat_core.is_cont_table == 0) {
        // Setup rows, minimum 10x10 grid
        for (var i = 1; i < drupalSettings.quadstat_core.data.length || i < 11; i++) {
          var d = (data[i - 1] = {});
          d["id"] = i - 1;
          d[0] = typeof drupalSettings.quadstat_core.data[i] != 'undefined' && typeof drupalSettings.quadstat_core.data[i][0] !== 'undefined' ? drupalSettings.quadstat_core.data[i][0] : i;
          for(var j = 0; (typeof drupalSettings.quadstat_core.data[i] !== 'undefined' && j < drupalSettings.quadstat_core.data[i].length + 1) || j < 11; j++) {
            d[j+1]  = (typeof drupalSettings.quadstat_core.data[i] !== 'undefined' && typeof drupalSettings.quadstat_core.data[i][j] !== 'undefined') ? drupalSettings.quadstat_core.data[i][j] : '';
          }
        }
      } else {
        for (var i = 1; i < drupalSettings.quadstat_core.data.length; i++) {
          var d = (data[i - 1] = {});
          d["id"] = i - 1;
          d[0] = drupalSettings.quadstat_core.data[i][0];
          for(var j = 0; (typeof drupalSettings.quadstat_core.data[i] !== 'undefined' && j < drupalSettings.quadstat_core.data[i].length + 2); j++) {
            d[j+1]  = (typeof drupalSettings.quadstat_core.data[i] !== 'undefined' && typeof drupalSettings.quadstat_core.data[i][j+1] !== 'undefined') ? drupalSettings.quadstat_core.data[i][j+1] : '';
          }
        } 
      }
      // Create the grid
      dataView = new Slick.Data.DataView();
      grid = new Slick.Grid("#quadstat-slickgrid", dataView, columns, options);
      grid.setSelectionModel(new Slick.RowSelectionModel());

      // wire up model events to drive the grid
      dataView.onRowCountChanged.subscribe(function (e, args) {
        grid.updateRowCount();
        grid.render();
      });
  
      dataView.onRowsChanged.subscribe(function (e, args) {
        grid.invalidateRows(args.rows);
        grid.render();
      });

      $(grid.getHeaderRow()).delegate(":input", "change keyup", function (e) {
        // filter if text is entered
        var columnId = $(this).data("columnId");
        if (columnId != null) {
          columnFilters[columnId] = $.trim($(this).val());
          dataView.refresh();
        }
      });

      grid.onHeaderRowCellRendered.subscribe(function(e, args) {
        // Add filters
        $(args.node).empty();
        $("<input type='text'>")
        .data("columnId", args.column.id)
        .val(columnFilters[args.column.id])
        .appendTo(args.node);
      });

      grid.onCellChange.subscribe(function (e, args) {
        dataView.updateItem(args.item.id, args.item);
        var pop = commandQueue.pop();
        if(typeof(pop) != 'undefined') {
          $('#edit-field-dataset-history-0-value').val($('#edit-field-dataset-history-0-value').val() + "update," + (pop['row'] + 1) + ',' + pop['cell'] + ',' + pop['serializedValue'] + "\n");
        }
        commandQueue.push(pop);
      });

      $('#quadstat-slickgrid-add-col').click(function() {
        // Add new column to end of grid
        columns = grid.getColumns();
        columns.push({id:columns.length, name: columns.length, field: columns.length, editor:Slick.Editors.Text});
        grid.setColumns(columns);
        $('#edit-field-dataset-history-0-value').val($('#edit-field-dataset-history-0-value').val() + "addcol," + drupalSettings.quadstat_core.longest_row + "\n");
      });

      $('#quadstat-slickgrid-add-row').click(function() {
        // Add a new row to bottom of grid
        var item = { id:data.length+1, 0:data.length+1 };
        dataView.addItem(item);
        grid.invalidate();
        grid.updateRowCount();
        grid.render();
        grid.scrollRowIntoView(data.length)
        $('#edit-field-dataset-history-0-value').val($('#edit-field-dataset-history-0-value').val() + "addrow,\n");
      });

      grid.init();
      // initialize the model after all the events have been hooked up
      dataView.beginUpdate();
      dataView.setItems(data);
      dataView.setFilter(filter);
      dataView.endUpdate();
      // if you don't want the items that are not visible (due to being filtered out
      // or being on a different page) to stay selected, pass 'false' to the second arg
      dataView.syncGridSelection(grid, true);

      if($('body.page-node-type-dataset').length && !$('form#node-dataset-edit-form').length) {
        $('#quadstat-slickgrid .slick-viewport div').on('click', 'div', function() {
          return false;
        });
        $('#quadstat-slickgrid .slick-viewport div').on('dblclick', 'div', function() {
          return false;
        });
      }

      $('.slick-header-column').click(function() {
        // Don't do anything if we're not editing
        if($('body.page-node-type-dataset').length && !$('form#node-dataset-edit-form').length) {
          return false;
        }
        // Get the position of the column name to change
        var nth = $(this).index();
        if(nth == 0) {
          return false;
        }
        var $renameColumnDialog = $('<div id="quadstat-dialog-rename-col"><p>Enter the new value for this column.</p><p><input type="text" value="" id="quadstat-slickgrid-rename-column-input"></p></div>').appendTo('body');
        Drupal.dialog($renameColumnDialog, {
          title: 'Rename Column',
            buttons: [{
              text: 'Save',
              click: function() {
                $(this).dialog('close');
                updateColumnHeaders(nth);
                $('#edit-field-dataset-history-0-value').val($('#edit-field-dataset-history-0-value').val() + "rename," + nth + "," + $('#quadstat-slickgrid-rename-column-input').val() + "\n");
              }
            }]
        }).showModal();
      });

      function updateColumnHeaders(nth) {
        var cols = grid.getColumns();
        cols[nth].name = $('#quadstat-slickgrid-rename-column-input').val();
        grid.setColumns(cols);
        redo_click_action();
      }

      function redo_click_action() {
        // Rebind click header column dialog
        $('.slick-header-column').click(function() {
          // Don't do anything if we're not editing
          if($('body.page-node-type-dataset').length && !$('form#node-dataset-edit-form').length) {
            return false;
          }
          // Get the position of the column name to change
          var nth = $(this).index();

          $('#quadstat-slickgrid-rename-column-input').val('');
          var $renameColumnDialog = $('#quadstat-dialog-rename-col');
          Drupal.dialog($renameColumnDialog, {
            title: 'Rename Column',
              buttons: [{
                text: 'Save',
                click: function() {
                  $(this).dialog('close');
                  updateColumnHeaders(nth);
                  $('#edit-field-dataset-history-0-value').val($('#edit-field-dataset-history-0-value').val() + "rename," + nth + "," + $('#quadstat-slickgrid-rename-column-input').val() + "\n");
                }
              }]
          }).showModal();
        });
      }

      if (typeof drupalSettings.quadstat_core.dataset_fid !== 'undefined') {
        $('.form-item-x input').val(drupalSettings.quadstat_core.dataset_fid);
      }

      $('#block-seven-content article footer').after($('.view-operation-viewer .view-content, .view-id-embedded_learning_apps .view-content'));

      /* Setup operation viewer for analysis on dataset pages */
      var m = 0;
      var webform_html = '<div id="dataset-operations"><select id="webform-app-select"><option value="" selected="selected">Analysis</option>';
      while (typeof drupalSettings.quadstat_core.webforms[m] !== 'undefined') {
        webform_html += '<option value="' + drupalSettings.quadstat_core.webforms[m]['nid'] + '">' + drupalSettings.quadstat_core.webforms[m]['title'] + '</option>';
        m++;
      }
      webform_html += '</select></div>';
      if (!$('.view-operation-viewer select#webform-app-select').length) {
        $('.view-operation-viewer').prepend(webform_html);
      }

      /* When the select changes load the appropriate operation panel */
      $('#webform-app-select').on('change', function() {
        $('.view-content').css('display', 'block');
        if ($('.view-content article:not([about="/node/' + $(this).val() + '"])').is(":visible") == true) {
          $('.view-content article:not([about="/node/' + $(this).val() + '"])').slideUp();
        }
        if ($('.view-content article[about="/node/' + $(this).val() + '"]').is(":visible") == false) {
          $('.view-content article[about="/node/' + $(this).val() + '"]').slideDown();
        }
        $('#learning-app-select').val('');
      });

      /* Setup operation viewer for learning on dataset pages */
      m = 0;
      var learning_html = '<select id="learning-app-select"><option value="" selected="selected">Instructional</option>';
      while (typeof drupalSettings.quadstat_core.learning[m] !== 'undefined') {
        learning_html += '<option value="' + drupalSettings.quadstat_core.learning[m]['nid'] + '">' + drupalSettings.quadstat_core.learning[m]['title'] + '</option>';
        m++;
      }
      learning_html += '</select>';
      if (!$('#dataset-operations select#learning-app-select').length) {
        $('#dataset-operations').prepend(learning_html);
      }

      /* When the select changes load the appropriate operation panel */
      $('#learning-app-select').on('change', function() {
        $('.view-content').css('display', 'block');
        if ($('.view-content article:not([about="' + drupalSettings.quadstat_core.aliases[$(this).val()] + '"])').is(":visible") == true) {
          $('.view-content article:not([about="' + drupalSettings.quadstat_core.aliases[$(this).val()] + '"])').slideUp();
        }
        if ($('.view-content article[about="' + drupalSettings.quadstat_core.aliases[$(this).val()] + '"]').is(":visible") == false) {
          $('.view-content article[about="' + drupalSettings.quadstat_core.aliases[$(this).val()] + '"]').slideDown();
        }
        $('#webform-app-select').val('');
      });

      // Function to highlight columns upon click
      function highlight(col) {
        if (col == 0) {
          alert('The "Rows" column may not be selected.');
          return true;
        }

        if ($('article article').is(":visible") == false) {
          return false;
        }

        if($('div.slick-header-column:eq(' + col + ')').hasClass('highlighted-col')) {
          // column already highlighted, deselect
          $('.slick-header-column').removeClass('highlighted-col');
          $('.slick-header-column').removeClass('highlighted-col-1');
          $('.slick-header-column').removeClass('highlighted-col-2');
          $('.slick-header-column').removeClass('highlighted-col-3');
          $('.slick-cell').removeClass('highlighted-col-1');
          $('.slick-cell').removeClass('highlighted-col-2');
          $('.slick-cell').removeClass('highlighted-col-3');
          $('.slick-cell').removeClass('highlighted-col');
          $('.form-item-v1 input').val('-1');
          $('.form-item-v2 input').val('-1');
          $('.form-item-v3 input').val('-1');
          // Populate the independent and dependent variable indicators
          $('.form-item--x- input').val('');
          $('.form-item--y- input').val('');
          return true;
        }
        if($('div.slick-header-column.highlighted-col').length == 0) {
          // no highlighted column, select first one
          $('.slick-cell.r' + col).addClass('highlighted-col');
          $('.slick-cell.r' + col).addClass('highlighted-col-1');
          $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col');
          $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col-1');
          $('.form-item-v1 input').val(col);
          // Populate the independent and dependent variable indicators
          $('.form-item--x- input').val($('.slick-header-column:eq(' + col + ') .slick-column-name').text());
          return true;
        }
        if($('div.slick-header-column.highlighted-col').length == 1 && $('input[data-drupal-selector="edit-v2"]').prop('required')) {
          // highlight 2nd column
          $('.slick-cell.r' + col).addClass('highlighted-col-2');
          $('.slick-cell.r' + col).addClass('highlighted-col');
          $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col');
          $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col-2');
          $('.form-item-v2 input').val(col);
          // Populate the independent and dependent variable indicators
          $('.form-item--y- input').val($('.slick-header-column:eq(' + col + ') .slick-column-name').text());
            return true;
        }
        if($('div.slick-header-column.highlighted-col').length == 2 && $('input[data-drupal-selector="edit-v3"]').prop('required')) {
            // highlight 3rd column (not in use)
            $('.slick-cell.r' + col).addClass('highlighted-col-3');
            $('.slick-cell.r' + col).addClass('highlighted-col');
            $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col');
            $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col-3');
            $('.form-item-v3 input').val(col);
            return true;
        }
      }
      $('.slick-header-column').on('click', function() {
        var col = $(this).index();
        highlight(col);
      });

      // Hide HTML link if there is no file
      if ($('#quadstat-html a').attr('href') != '[webform_submission:values:html:raw]') {
        $('#quadstat-html').css('display', 'block');
        $('#quadstat-pdf').css('display', 'block');
      } else {
        $('#quadstat-html').css('display', 'none');
        $('#quadstat-pdf').css('display', 'none');
      }

      // Hide R File link if there is no file
      if ($('#quadstat-r-file a').attr('href') != '[webform_submission:values:r_file]') {
        $('#quadstat-r-file').css('display', 'block');
      } else {
        $('#quadstat-r-file').css('display', 'none');
      }

      if ($('#quadstat-shorten a').length) {
        if ($('#quadstat-shorten a').attr('href').match('bit.ly')) {
          $('#quadstat-shorten').css('display', 'block');
        } else {
          $('#quadstat-shorten').css('display', 'none');
        }
      }

      jQuery(document).ajaxComplete(function(event, xhr, settings) {
        $('.form-item-x input').val(drupalSettings.quadstat_core.dataset_fid);
        // This is a hack, I don't know why the image is being src'd as httpss
        if ($('img.statistics_image').length) {
          $('img.statistics_image').attr('src', $('img.statistics_image').attr('src').replace('httpss', 'https'));
          if ($('img.statistics_image').attr('src') == '[webform_submission:values:image]') {
            $('img.statistics_image').hide();
          }
        }
      });

      // Hide the analysis if requested
      $('.form-item.js-form-type-processed-text.form-type-processed-text,#edit-clear-analysis').on('click', function() {
        $('.messages.messages--status').slideUp(2000);
        $('.messages.messages--error').slideUp(1000);
      });

      // Remove empty <p></p>
      $('p').filter(function () { return $.trim(this.innerHTML) == "" }).remove();
    }   
  };
}(jQuery, Drupal, document));
