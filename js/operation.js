/**
 * @file
 * Javascript behaviors for performing R operations.
 */

/* global Drupal, jQuery, document */

(function ($, Drupal, document) {

  'use strict';

  /**
   * Attaches behaviors for Quadstat.
   */
  Drupal.behaviors.quadstat_core = {
    attach: function (context, settings) {

      // Hide HTML link if there is no file
      if ($('#quadstat-html a').attr('href') != '[webform_submission:values:html:raw]') {
        $('#quadstat-html').css('display', 'block');
        $('#quadstat-pdf').css('display', 'block');
      } else {
        $('#quadstat-html').css('display', 'none');
        $('#quadstat-pdf').css('display', 'none');
      }

      if ($('#quadstat-shorten a').length) {
        if ($('#quadstat-shorten a').attr('href').match('bit.ly')) {
          $('#quadstat-shorten').css('display', 'block');
        } else {
          $('#quadstat-shorten').css('display', 'none');
        }
      }

      if ($('#quadstat-r-file a').length) {
        if ($('#quadstat-r-file a').attr('href').match('quadstat')) {
          $('#quadstat-r-file').css('display', 'block');
        } else {
          $('#quadstat-r-file').css('display', 'none');
        }
      }

      var fid;
      var v1;

      jQuery(document).ajaxStart(function() {
        fid = $('.form-item-x input').val();
      });

      jQuery(document).ajaxComplete(function(event, xhr, settings) {
        $('.form-item-x input').val(fid);
        // This is a hack, I don't know why the image is being src'd as httpss
        if ($('img.statistics_image').length) {
          $('img.statistics_image').attr('src', $('img.statistics_image').attr('src').replace('httpss', 'https'));
        }
      });

      // Remove quotes from input
      $('#views-exposed-form-dataset-block-1 .form-text').change(function() {
        $(this).val(function(i, v) { 
          return v.replace(/\"/g, '');
        });
      });

      $('input[data-drupal-selector="edit-operation-dataset-edit"]').on('click', function(e) {
        e.stopPropagation();
        e.preventDefault();
        var nid = $('.views-field-nid').text().trim();
        window.location.href = '/node/' + nid; 
        return false;
      });

      $('#edit-operation-dataset-edit').removeClass('js-form-submit form-submit').prop("type", "button");

      // Function to highlight columns upon click
      function highlight(col) {
        if (col == 0) {
          alert('The "Rows" column may not be selected.');
          return true;
        }
        if($('div.slick-header-column:eq(' + col + ')').hasClass('highlighted-col')) {
          // column already highlighted, deselect
          $('.slick-header-column').removeClass('highlighted-col');
          $('.slick-header-column').removeClass('highlighted-col-1');
          $('.slick-header-column').removeClass('highlighted-col-2');
          $('.slick-header-column').removeClass('highlighted-col-3');
          $('.slick-cell').removeClass('highlighted-col-1');
          $('.slick-cell').removeClass('highlighted-col-2');
          $('.slick-cell').removeClass('highlighted-col-3');
          $('.slick-cell').removeClass('highlighted-col');
          $('.form-item-v1 input').val('-1');
          $('.form-item-v2 input').val('-1');
          $('.form-item-v3 input').val('-1');
          // Populate the independent and dependent variable indicators
          $('.form-item--x- input').val('');
          $('.form-item--y- input').val('');
          return true;
        }
        if($('div.slick-header-column.highlighted-col').length == 0) {
          // no highlighted column, select first one
          $('.slick-cell.r' + col).addClass('highlighted-col');
          $('.slick-cell.r' + col).addClass('highlighted-col-1');
          $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col');
          $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col-1');
          $('.form-item-v1 input').val(col);
          // Populate the independent and dependent variable indicators
          $('.form-item--x- input').val($('.slick-header-column:eq(' + col + ') .slick-column-name').text());
          return true;
        }
        if($('div.slick-header-column.highlighted-col').length == 1 && $('.form-item-v2 input').length) {
          // highlight 2nd column
          $('.slick-cell.r' + col).addClass('highlighted-col-2');
          $('.slick-cell.r' + col).addClass('highlighted-col');
          $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col');
          $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col-2');
          $('.form-item-v2 input').val(col);
          // Populate the independent and dependent variable indicators
          $('.form-item--y- input').val($('.slick-header-column:eq(' + col + ') .slick-column-name').text());
            return true;
        }
        if($('div.slick-header-column.highlighted-col').length == 2 && $('.form-item-v3 input')) {
            // highlight 3rd column (not in use)
            $('.slick-cell.r' + col).addClass('highlighted-col-3');
            $('.slick-cell.r' + col).addClass('highlighted-col');
            $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col');
            $('.slick-header-column:eq(' + col + ')').addClass('highlighted-col-3');
            $('.form-item-v3 input').val(col);
            return true;
        }
      }

      /*
       * The rest of the code in this file is for Slickgrid preview
       */

      var grid;
      var options;
      var data = [];
      var d = [];
      var columns = [];

      // Define options for grid
      var options = {
        editable: false,
        enableAddRow: false,
        enableCellNavigation: false,
        forceFitColumns: false,
        enableColumnReorder: false,
        showHeaderRow: false,
        autoedit: false,
      };

      // Setup columns
      if(typeof drupalSettings === 'undefined' || typeof drupalSettings.quadstat_core === 'undefined') {
        return;
      }
      for(var i = 0; i < drupalSettings.quadstat_core.data.length; i++) {
        if (drupalSettings.quadstat_core.is_cont_table[i] == 0) {
          // It's not a contingency table
          columns[i] = [{id:0, name:"Rows", field:0, cssClass:"cell-title", formatter: function(row){return row+1}}];
          for(var j = 0; j < drupalSettings.quadstat_core.data[i]['longest_row'] || j < 35; j++) { 
            columns[i].push({id: j, name: drupalSettings.quadstat_core.data[i]['val'][0][j], field:j});
          }
        } else {
          columns[i] = [{id:0, name:"Rows", field:0, cssClass:"cell-title"}];
          for(var j = 1; j < drupalSettings.quadstat_core.data[i]['longest_row'] || j < 35; j++) { 
            columns[i].push({id: j, name: drupalSettings.quadstat_core.data[i]['val'][0][j - 1], field:j});
          }
        }
      }
      function setItems(data, objectIdProperty) {
        if (objectIdProperty !== undefined) {
          idProperty = objectIdProperty;
        }
        items = data;
        refreshIdxById();
        refresh();
      }

      // Setup rows, minimum 10x10 grid
      for (var i = 0; i < drupalSettings.quadstat_core.data.length; i++ ) {
        data[i] = new Array();
        if (drupalSettings.quadstat_core.is_cont_table[i] == 0) {
          for (var j = 1; j < drupalSettings.quadstat_core.data[i]['val'].length; j++) {
            data[i][j - 1] = new Array();
            for (var k = 0; k < drupalSettings.quadstat_core.data[i].val[j].length; k++) {
              data[i][j - 1][k]  = drupalSettings.quadstat_core.data[i].val[j][k];
            }
          }
        } else {
          for (var j = 1; j < drupalSettings.quadstat_core.data[i]['val'].length; j++) {
            data[i][j - 1] = new Array();
            for (var k = 0; k < drupalSettings.quadstat_core.data[i].val[j].length; k++) {
              data[i][j - 1][k]  = drupalSettings.quadstat_core.data[i].val[j][k];
            }
          }
        }
      }

      if($('#quadstat-slickgrid').length && typeof columns[0] !== 'undefined') {
        grid = new Slick.Grid("#quadstat-slickgrid", data[0], columns[0], options);
        grid.init();  
      } else {
        return;
      }

      $('#block-views-block-dataset-block-1 table').on('dataset_changed', 'td.views-field.views-field-fid', load_new_dataset);
      function load_new_dataset() { 
        fid = Number($('td.views-field.views-field-fid').html());
        if (fid == '1162') {
          // Inline data was chosen
          if (!$('.form-item-inline-data').is(":visible")) {
            $('.form-item-inline-data').slideDown(2000);
            $('select[data-drupal-selector="edit-dataset-selected-custom"] option:selected').val('');
            $('#edit-dataset-last-selected').val('custom');
          }
          if ($('#quadstat-slickgrid').is(":visible")) {
            $('#quadstat-slickgrid').slideUp(2000);
          }
        } else {
          if ($('.form-item-inline-data').is(":visible")) {
            // Inline data was not chosen
            $('.form-item-inline-data').slideUp(2000);
            $('select[data-drupal-selector="edit-dataset-selected-custom"] option:selected').val('')
            $('#edit-dataset-last-selected').val('curated');
          }
          if (!$('#quadstat-slickgrid').is(":visible") && !$('form.no-dataset').length) {
            $('#quadstat-slickgrid').slideDown(2000);
          }
        }
        if ($('select[data-drupal-selector="edit-dataset-selected-custom"] option:selected').val() == '' && $('#edit-dataset-last-selected').val() == 'custom') {
          $('input[data-drupal-selector="edit-title-3"]').val('Example Data');
          $('select[data-drupal-selector="edit-dataset-selected"] option:selected').val('Example Data');
          fid = 244;
        }
        if (typeof columns[drupalSettings.quadstat_core.map[fid]] === 'undefined') {
          fid = 244;
        }
        // store for later use
        v1 = $('.form-item-v1 input').val();
        grid.invalidate();
        grid = new Slick.Grid("#quadstat-slickgrid", data[drupalSettings.quadstat_core.map[fid]], columns[drupalSettings.quadstat_core.map[fid]], options);
        grid.init();
        // Tell Quadstat which dataset to perform the operation on
        $('.form-item-x input').val(fid);

        if (($('.form-item-v input').length && $('.form-item-v input')[0].checked) || $('.form-item-v1 input').length == 0) {
          $('.webform-submission-form .form-submit').removeAttr('disabled');
          $('.webform-submission-form .form-submit').addClass('button js-form-submit form-submit');
        }
        // Highlight and validate
        $('.slick-header-column').on('click', function() { 
          var col = $(this).index();
          highlight(col);
        });
        $('input[data-drupal-selector="edit-operation-dataset-edit"]').removeClass('js-form-submit form-submit').prop("type", "button");
      }

      $('#block-views-block-dataset-block-1 td.views-field.views-field-fid').trigger('dataset_changed');

      if($('.no-dataset').length) {
        $('#block-views-block-dataset-block-1, #quadstat-slickgrid').css('display', 'none'); 
      }  

      $('.form-item.js-form-type-processed-text.form-type-processed-text,#edit-clear-analysis').on('click', function() { 
        $('.messages.messages--status').slideUp(2000);
        $('.messages.messages--error').slideUp(1000);
      });

    }
  };
}(jQuery, Drupal, document));
