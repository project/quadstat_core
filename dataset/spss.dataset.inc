<?php

/**
 * Process a dataset file uploaded by the user
 */
function _quadstat_process_spss_dataset(Drupal\Core\Entity\EntityInterface $entity) {

  $tmp_path = \Drupal::service('file_system')->realpath('temporary://');

  $user = \Drupal::currentUser();
  // Get the identifier for this request
  $identifier = _quadstat_core_get_identifier();

  // get the filepath of the unprocessed upload file
  $file = $entity->get('field_dataset_file')->referencedEntities();
  $uri = $file[0]->get('uri')->get(0)->value;
  $filepath = file_unmanaged_copy($uri, 'temporary://', FILE_EXISTS_REPLACE);
  $filepath = \Drupal::service('file_system')->realpath($filepath);
  // put a newline at end of file so R doesn't throw an error
  file_put_contents($filepath, file_get_contents($filepath) . "\n");
  $csv = "$tmp_path/dataset-$identifier.csv";
  // delete the CSV file if it exists from a previous request
  if (is_file($csv)) {
    unlink($csv);
  }

// Get R code ready to save
$commands = <<<RCODE
library(foreign)
user_table <- read.spss("$filepath");
write.table(user_table, file = "$csv", append = FALSE, quote = FALSE, sep = ",", na = "", dec = ".", col.names = TRUE, row.names = FALSE);
RCODE;

  // Run the code
  _quadstat_execute_r($commands, $identifier);

  // Attach created dataset to node
  file_delete($file[0]->get('fid')->get(0)->value);
  if(is_file($csv)) {
    $dataset = file_get_contents($csv);
    $datafile = _quadstat_save_file($dataset, $user, $identifier);
    if($datafile) {
      $entity->field_dataset_file->setValue(['target_id' => $datafile->get('fid')->get(0)->value]);
    } 
  }
  // Let user know of any errors or warnings while processing dataset request
  if(is_file($error_file = "$tmp_path/dataset-$identifier.err") && filesize($error_file) > 0) {
    drupal_set_message(file_get_contents($error_file), 'error');
  }
  if(is_file($warn_file = "$tmp_path/dataset-$identifier.warn") && filesize($warn_file) > 0) {
    drupal_set_message(file_get_contents($warn_file), 'warning');
  }
}
